package Exercise_01;

public class Main {
    public static void main(String[] args) {
        System.out.println("The Users for access are:\n-------------------------------------");
        User username = new User("Admin", "Popescu","Andrei");
        username.printing();
        User username2 = new User("Student", "Enescu", "Mihai");
        username2.printing();
        User username3 = new User("Tester", "Vianu", "Bianca");
        username3.printing();

        username2.generateDisplayName("Admin");
        System.out.println("NOW: Changing Student username with Admin username!");
        username2.printing();
    }
}
