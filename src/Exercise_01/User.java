package Exercise_01;

/* Encapsulation:
• Create a class User with the following fields: username, firstName, lastName.
• Make the fields private
• Write getters and setters
• Write a constructor with 3 parameters
• Make the object immutable
• Write a public method generateDisplayName that returns a string with the full name of the user.
 */
public class User {
    private String username;
    private String firstName;
    private String lastName;

    public User(String username, String firstName, String lastName) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public void printing() {
        System.out.println("Username: " + username + "\nFirst Name: " + firstName + "\nLast Name: " + lastName);
        System.out.println("------------------");
    }

    public void generateDisplayName(String newUser) {
        this.username = newUser;
    }
}
